const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');

const port = 3000
const app = express()

app.use(cors({ origin: '*'})) // почитай про CORS
app.use(bodyParser.json());

app.get('/', (req, res) => {
    res.send('Hello World!');
});

app.post('/data', (req, res) => {
    console.clear();
    console.log("++++++++++++++++++++++++++")
    console.log(req.body)
    console.log("==========================")
})

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})
